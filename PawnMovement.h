/*
 * PawnMovement.h
 *
 *  Created on: Sep 17, 2015
 *      Author: anvik
 */

#ifndef PAWNMOVEMENT_H_
#define PAWNMOVEMENT_H_

#include "Movement.h"

class WhitePawnMovement: public Movement {
public:
	WhitePawnMovement(){};
	virtual ~WhitePawnMovement(){};
	std::vector<const Coord*> path(const Coord* s, const Coord* e);
};



class BlackPawnMovement: public Movement {
public:
	BlackPawnMovement(){};
	virtual ~BlackPawnMovement(){};
	std::vector<const Coord*> path(const Coord* s, const Coord* e);
};

#endif /* PAWNMOVEMENT_H_ */