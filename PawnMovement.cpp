/*
 * PawnMovement.cpp
 *
 *  Created on: Sep 17, 2015
 *      Author: anvik
 */

#include "PawnMovement.h"
#include "Exceptions.h"
#include <math.h>
#include "Game.h"


std::vector<const Coord*> WhitePawnMovement::path(const Coord* s, const Coord* e) {

	if(s->x >= e->x)
	{
		throw invalid_move_error("White pawns move down");
	}
	if (s->y != e->y)
	{		
		throw invalid_move_error("Pawns move vertically");
	}
	double distance = sqrt((abs(s->x - e->x) + abs(s->y - e->y)));
	if (distance > 1)
	{	
		throw invalid_move_error("Pawns move only 1 space");
	}
	std::vector<const Coord*> p;
	p.push_back(new Coord(s->x, s->y));
	p.push_back(new Coord(e->x, e->y));

	return p;
}

std::vector<const Coord*> BlackPawnMovement::path(const Coord* s, const Coord* e) {

	if(s->x <= e->x)
	{
		throw invalid_move_error("Black pawns move up");
	}
	if (s->y != e->y)
	{		
		throw invalid_move_error("Pawns move vertically");
	}
	double distance = sqrt((abs(s->x - e->x) + abs(s->y - e->y)));
	if (distance > 1)
		throw invalid_move_error("Pawns move only 1 space");
	std::vector<const Coord*> p;
	p.push_back(new Coord(s->x, s->y));
	p.push_back(new Coord(e->x, e->y));

	return p;
}
