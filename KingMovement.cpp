/*
 * KingMovement.cpp
 *
 *  Created on: Sep 17, 2015
 *      Author: anvik
 */

#include "KingMovement.h"
#include "Exceptions.h"
#include <math.h>

std::vector<const Coord*> KingMovement::path(const Coord* s, const Coord* e) {


	double distance = sqrt((abs(s->x - e->x) + abs(s->y - e->y)));
	if(distance > 1)
	{
		if(s->x == e->x || s->y == e->y || distance > 1.5)
		{
			throw invalid_move_error ("Kings can only move 1 space.");
		}

	}
	std::vector<const Coord*> path;
	path.push_back(new Coord(s->x, s->y));
	path.push_back(new Coord(e->x, e->y));
	return path;
}
