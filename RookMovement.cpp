/*
 * RookMovement.cpp
 *
 *  Created on: Sep 17, 2015
 *      Author: anvik
 */

#include "RookMovement.h"
#include "Exceptions.h"

std::vector<const Coord*> RookMovement::path(const Coord* s, const Coord* e) {
	if (s->x != e->x && s->y != e->y)
	{
		throw invalid_move_error("Rooks can only move vertically or horizontally");
	}
	std::vector<const Coord*> path;
	
	int rowDiff = (e->x - s->x);
	int colDiff = (e->y - s->y);

	int rowCount = s->x;
	int colCount = s->y;

	path.push_back(new Coord(s->x, s->y));
///HANDLE HORIZONTAL MOVEMENT
	//path.push_back(s);

	if(rowDiff == 0)
	{
		if(colDiff > 0)
		{
			for(int i = 0; i < colDiff; i++)
			{
				Coord* c = new Coord(rowCount, colCount+1);
				path.push_back(c);
				colCount++;
			}
		}
		if(colDiff < 0)
		{
			for(int i = (s->y -1); i > (e->y -1); i--)
			{
				Coord* c = new Coord(s->x, i);
				path.push_back(c);
			}
		}
	}

///HANDLE VERTICAL MOVEMENT

	if(colDiff == 0)
	{
		if(rowDiff > 0)
		{
			for(int i = 0; i < rowDiff; i++)
			{
				Coord* c = new Coord(rowCount+1, colCount);
				path.push_back(c);
				rowCount++;
			}
		}
		if(rowDiff < 0)
		{
			for(int i = 0; i< abs(rowDiff); i++)
			{
				Coord* c = new Coord(rowCount-1, colCount);
				path.push_back(c);
				rowCount--;
			}
		}
	}


	return path;
}

