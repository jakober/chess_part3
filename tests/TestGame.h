#ifndef TESTGAME_H
#define TESTGAME_H

#include <cppunit/extensions/HelperMacros.h>

class TestGame : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( TestGame );
  CPPUNIT_TEST( test_Game );
  CPPUNIT_TEST_SUITE_END();

public:

  Game *chess;

  void setUp();
  void tearDown();


  void test_Chess_Constructor();
  void test_Chess_SetUp();
  void test_Chess_IsOver();
  void test_Chess_getSquare();

  void test_Game();

};

#endif  // TESTGAME_H