#include "QueenMovement.h"
#include "PawnMovement.h"

#include "Game.h"
#include "ChessBoard.h"
#include "TestBoard.h"

#include "Exceptions.h"
#include <iostream>

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestBoard );

void TestBoard::setUp()
{
	blackQueen1 = new Piece(Piece::black, 'Q', new QueenMovement);
	blackQueen2 = new Piece(Piece::black, 'Q', new QueenMovement);
	whiteQueen1 = new Piece(Piece::white, 'q', new QueenMovement);

	blackPawn1 = new Piece(Piece::black, 'P', new BlackPawnMovement);
	whitePawn1 = new Piece(Piece::white, 'p', new WhitePawnMovement);

	board = new Board(3,3);
	chessBoard = new ChessBoard(3,3);

}


void TestBoard::tearDown()
{
	delete blackQueen1;
	delete blackQueen2;
	delete whiteQueen1;
	delete blackPawn1;
	delete whitePawn1;
	delete board;
	delete chessBoard;
}

void TestBoard::test_getSquare()
{
	/*
	 *Ensure errors are thrown for all coordinates that are out of bounds for a Board
	 */
	///Check negative row, valid column
	CPPUNIT_ASSERT_THROW(board->getSquare(-1, 0), invalid_coordinates_error);
	///Check negative row and negative column
	CPPUNIT_ASSERT_THROW(board->getSquare(-1, -1), invalid_coordinates_error);
	///Check valid row and negative column
	CPPUNIT_ASSERT_THROW(board->getSquare(0, -1), invalid_coordinates_error);
	///Check 1 larger than the largest valid row value and a valid column
	CPPUNIT_ASSERT_THROW(board->getSquare(board->width, 0), invalid_coordinates_error);
	///Check a valid row and 1 larger than the largest valid column value
	CPPUNIT_ASSERT_THROW(board->getSquare(0, board->height), invalid_coordinates_error);
	///Check 1 larger than the largest valid row value and 1 larger than the largest valid column value 
	CPPUNIT_ASSERT_THROW(board->getSquare(board->width, board->height), invalid_coordinates_error);
	
	/*
	 *Ensure that there are no invalid_coordinate_errors thrown for every valid call of getSquare()
	 *Iterates through every row and column to ensure that nothing is thrown by getSquare for valid
	 *row and column values
	 */
	for(int i = 0; i < board->width; i++)
	{
		for(int j = 0; j < board->height; j++)
		{
			CPPUNIT_ASSERT_NO_THROW(board->getSquare(i,j));
		}

	}


	
}

void TestBoard::test_boardConstructor(int w, int h)
{
	///Check that width and height are properly assigned during construction
	CPPUNIT_ASSERT_EQUAL(w, board->width);
	CPPUNIT_ASSERT_EQUAL(h, board->height);
	
	/*
	 *Traverse every square on the board and ensure that the row, column, and 
	 *Piece pointer are correctly assigned so they can be appropriately referenced by
	 *other Board methods such as getSquare().
	 */
	for(int i = 0; i < w; i++)
	{
		for(int j = 0; j < h; j++)
		{
			///Check that the square's row value equals the board's row
			CPPUNIT_ASSERT_EQUAL(i, board->getSquare(i,j)->row);
			CPPUNIT_ASSERT_EQUAL(j, board->getSquare(i,j)->col);
			CPPUNIT_ASSERT(board->getSquare(i,j)->getPiece() == NULL);
		}
	}
}
void TestBoard::test_boardDestructor()
{

}
void TestBoard::test_draw()
{
	std::ostringstream drawOutput;
	///This is what draw should output for our 3 x 3 board
	std::string s = " 012\n0...\n1...\n2...\n";
	///Output the draw method to ostringstream drawOutput
	board->draw(drawOutput);
	///Compare the expected output string to the actual output of board->draw()
	CPPUNIT_ASSERT_EQUAL(s, drawOutput.str());
}
void TestBoard::test_pieceMethods()
{
	///Kill the piece
	whiteQueen1->kill();
	///Check that it has actually been killed
	CPPUNIT_ASSERT_EQUAL(false, whiteQueen1->isAlive());
	delete whiteQueen1;
	///Ensure that it is alive and ready to be used again in other tests
	///Have to recreate the piece since alive is a private variable with no setter function
	Piece *whiteQueen1 = new Piece(Piece::white, 'q', new QueenMovement);
	CPPUNIT_ASSERT_EQUAL(true, whiteQueen1->isAlive());
}

/*
 *Board's methods use Square's smethods so we must test these first
 */
void TestBoard::test_squareMethods()
{	///Check that Square's basic constructor properly initializes values
	Square square = Square(3,3);
	CPPUNIT_ASSERT_EQUAL(3, square.row);
	CPPUNIT_ASSERT_EQUAL(3, square.col);
	CPPUNIT_ASSERT(square.getPiece()==NULL);


	Square* s = board->getSquare(0,0);

	///Test that you cannot set a piece on a piece of your own color
	s->setPiece(whiteQueen1);
	CPPUNIT_ASSERT_THROW(s->setPiece(whiteQueen1), invalid_move_error);

	///Test that you can replace a piece of a different colour
	CPPUNIT_ASSERT_NO_THROW(s->setPiece(blackQueen1));
	CPPUNIT_ASSERT(s->getPiece()==blackQueen1);

	///Test remove piece makes the Square's piece pointer null
	s->removePiece();
	CPPUNIT_ASSERT(s->getPiece()==NULL);

	///Test that the symbol is properly returned of a given Square's piece
	CPPUNIT_ASSERT_EQUAL(EMPTY, s->symbol()); 
	s->setPiece(whiteQueen1);
	CPPUNIT_ASSERT_EQUAL('q', s->symbol());
	s->removePiece();
}

void TestBoard::test_placePiece()
{
	///Since all Square methods have been tested, we can test Board methods which use Square methods
	Square *s = board->getSquare(0,0);
	///Ensure the square we start with is empty.
	s->removePiece();
	///Place a piece on the empty square. Ensure no error is thrown
	CPPUNIT_ASSERT_NO_THROW(board->placePiece(blackQueen1, s));
	///Attempt to place a piece of the same colour on the occupied square, ensure error is thrown
	CPPUNIT_ASSERT_THROW(board->placePiece(blackQueen2, s), invalid_move_error);
	///Attempt to place a piece of the opposite colour on the occupied square, ensure error is not thrown
	CPPUNIT_ASSERT_NO_THROW(board->placePiece(whiteQueen1, s));
	///Check that the square actually has had it's piece pointer change to whiteQueen1 during the kill
	CPPUNIT_ASSERT_EQUAL(whiteQueen1, s->getPiece());
}

void TestBoard::test_movePiece()
{
	/*
	 *First we will test all error checking that takes place before the piece is actually moved
	 */
	Square square1 = Square(0,0);
	Square square2 = Square(1,1);
	Square* s1 = &square1;
	Square* s2 = &square2;
	
	///Check with invalid starting square row
	square1.row = -1;
	CPPUNIT_ASSERT_THROW(board->movePiece(s1, s2), invalid_coordinates_error);
	square1.row = board->width;
	CPPUNIT_ASSERT_THROW(board->movePiece(s1, s2), invalid_coordinates_error);
	square1.row = 0;

	///Check with invalid starting square column
	square1.col = -1;
	CPPUNIT_ASSERT_THROW(board->movePiece(s1, s2), invalid_coordinates_error);
	square1.col = board->height;
	CPPUNIT_ASSERT_THROW(board->movePiece(s1, s2), invalid_coordinates_error);
	square1.col = 0;

	///Check with invalid ending square row
	square2.row = -1;
	CPPUNIT_ASSERT_THROW(board->movePiece(s1, s2), invalid_coordinates_error);
	square2.row = board->width;
	CPPUNIT_ASSERT_THROW(board->movePiece(s1, s2), invalid_coordinates_error);
	square2.row = 1;

	///Check with invalid ending square column
	square2.col = -1;
	CPPUNIT_ASSERT_THROW(board->movePiece(s1, s2), invalid_coordinates_error);
	square2.col = board->height;
	CPPUNIT_ASSERT_THROW(board->movePiece(s1, s2), invalid_coordinates_error);
	square2.col = 1;


	///Test that an error is thrown when no piece exists on starting square
	s1 = board->getSquare(0,0);
	s2 = board->getSquare(1,1);
	///Ensure error is thrown for empty s1
	s1->removePiece();
	CPPUNIT_ASSERT_THROW(board->movePiece(s1,s2), invalid_piece_error);
	

	/*
	 *Test that a valid move will be executed if no errors were thrown.
	 *That is, ensure that s2->piece is equal to what s1->piece equalled at
	 *the beginning of the function call. Also check that s1->piece equals
	 *NULL following the execution of movePiece(s1, s2)
	 *NOTE: we do not have to check that moving to s2 is a valid move
	 *for the piece on s1 since this is the responsibility of ChessBoard's
	 *movePiece(), not Board's movePiece().
	 */
	 s1->setPiece(blackQueen1);
	 CPPUNIT_ASSERT_NO_THROW(board->movePiece(s1,s2));
	 CPPUNIT_ASSERT(s1->getPiece()==NULL);
	 CPPUNIT_ASSERT_EQUAL(blackQueen1, s2->getPiece()); 
	

}

void TestBoard::test_ChessBoard_movePiece()
{
	/*
	 * Here we check that the bug, where pieces could be killed with certain invalid moves,
	 * has been fixed and that the after calling ChessBoard's movePiece() with a pawn moving
	 * two spaces to kill another pawn, not only throws an error, but also that the destination
	 * square still holds the piece it held prior to the invalid call of movePiece()
	 */
	Square *s1 = chessBoard->getSquare(0,0);
	Square *s2 = chessBoard->getSquare(2,2);
	chessBoard->placePiece(whitePawn1, s1);
	chessBoard->placePiece(blackPawn1, s2);
	///Set the test to be that it is White's turn
	Game::turn = 0;
	///Make sure the error is thrown by PawnMovement "Pawns move vertically"
	CPPUNIT_ASSERT_THROW(chessBoard->movePiece(s1, s2), invalid_move_error);
	///Make sure the error is thrown by PawnMovement "Pawns move only 1 space"
	CPPUNIT_ASSERT_THROW(chessBoard->movePiece(s1, chessBoard->getSquare(2,0)), invalid_move_error);
	///Make sure that the destination square has not had the piece removed
	CPPUNIT_ASSERT_EQUAL(blackPawn1 ,s2->getPiece());

}

void TestBoard::test_ChessBoard_Player_Turn()
{
	///place a black piece on (0,0), try to move it, and ensure error is thrown if it is white's turn
	Game::turn = 0;
	chessBoard->placePiece(blackQueen1, chessBoard->getSquare(0,0));
	CPPUNIT_ASSERT_THROW(chessBoard->movePiece(chessBoard->getSquare(0,0), chessBoard->getSquare(1,1)), invalid_move_error);
	blackQueen1 = chessBoard->getSquare(0,0)->removePiece();

	///Place a white piece on (0,0), try to move it, and ensure error is thrown if it is black's turn
	Game::turn = 1;
	chessBoard->placePiece(whiteQueen1, chessBoard->getSquare(0,0));
	CPPUNIT_ASSERT_THROW(chessBoard->movePiece(chessBoard->getSquare(0,0), chessBoard->getSquare(1,1)), invalid_move_error);
	whiteQueen1 = chessBoard->getSquare(0,0)->removePiece();

}
void TestBoard::test_ChessBoard_Jump_Pieces()
{
	///Populate the board with two pieces
	chessBoard->placePiece(blackQueen1, chessBoard->getSquare(0,0));
	chessBoard->placePiece(whiteQueen1, chessBoard->getSquare(1,1));
	///Ensure error is thrown if you attempt to jump the piece
	Game::turn = 1;
	CPPUNIT_ASSERT_THROW(chessBoard->movePiece(chessBoard->getSquare(0,0), chessBoard->getSquare(2,2)), invalid_move_error);
	///Ensure no error is thrown if move is valid/doesn't jump
	Game::turn = 1;
	//std::cout << chessBoard->getSquare(2,0)->getPiece()->player;
	//std::cout << chessBoard->getSquare(1,1)->getPiece()->player;
	CPPUNIT_ASSERT_NO_THROW(chessBoard->movePiece(chessBoard->getSquare(0,0),chessBoard->getSquare(2,0)));
	///depopulate the board for other test methods
	blackQueen1 = chessBoard->getSquare(2,0)->removePiece();
	whiteQueen1 = chessBoard->getSquare(1,1)->removePiece();
}





void TestBoard::test_Board()
{
	test_boardConstructor(3, 3);
	test_getSquare();
	test_pieceMethods();
	test_squareMethods();
	test_draw();
	test_placePiece();
	test_movePiece();

	test_ChessBoard_Jump_Pieces();
	test_ChessBoard_Player_Turn();
	test_ChessBoard_movePiece();
}