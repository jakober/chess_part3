#ifndef TESTBOARD_H
#define TESTBOARD_H

#include <cppunit/extensions/HelperMacros.h>

class TestBoard : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( TestBoard );
  CPPUNIT_TEST( test_Board );
  CPPUNIT_TEST_SUITE_END();

public:

  void setUp();
  void tearDown();
  Piece *blackQueen1;
  Piece *blackQueen2;
  Piece *whiteQueen1;

  Piece *whitePawn1;
  Piece *blackPawn1;
  
  Board *board;
  ChessBoard *chessBoard;

  void test_pieceMethods();
  void test_squareMethods();


  /*
   *Ensures that for every valid input(any valid row and col on board), no errors are thrown. Also
   *tests all edge cases to ensure that an invalid_coordinates_error is thrown when an in valid row,
   *invalid column, or both, are entered. Tests both at the upper and lower limits of the board's
   *dimensions.
   */ 
  void test_getSquare();

  ///Takes expected width and height as parameters and Asserts that they are equivalent to the Board's width and height
  void test_boardConstructor(int w, int h);
  void test_boardDestructor();

  void test_draw();
  void test_placePiece();
  void test_movePiece();
  void test_Board();


  void test_ChessBoard_movePiece();
  void test_ChessBoard_Player_Turn();
  void test_ChessBoard_Jump_Pieces();

};

#endif  // TESTBOARD_H