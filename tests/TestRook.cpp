#include "RookMovement.h"

#include "Game.h"
#include "TestRook.h"

#include "Exceptions.h"
#include <iostream>
#include <vector>

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestRook );

void TestRook::setUp()
{
	Rook = new Piece(Piece::black, 'R', new RookMovement);
	c00 = new Coord(0,0);
	c10 = new Coord(1,0);
	c20 = new Coord(2,0);
	c01 = new Coord(0,1);
	c11 = new Coord(1,1);
	c21 = new Coord(2,1);
	c02 = new Coord(0,2);
	c12 = new Coord(1,2);
	c22 = new Coord(2,2);

}


void TestRook::tearDown()
{
	delete Rook;
	delete c00;
	delete c10;
	delete c20;
	delete c01;
	delete c11;
	delete c21;
	delete c02;
	delete c12;
	delete c22;

}

void TestRook::test_diagonal()
{
	/*
	 *Test Rook diagonal movement
	 *up and left
	 */
	CPPUNIT_ASSERT_THROW(Rook->movement->path(c11, c00), invalid_move_error);
	///up and right
	CPPUNIT_ASSERT_THROW(Rook->movement->path(c11, c02), invalid_move_error);
	///down and left
	CPPUNIT_ASSERT_THROW(Rook->movement->path(c11, c20), invalid_move_error);
	///down and right
	CPPUNIT_ASSERT_THROW(Rook->movement->path(c11, c22), invalid_move_error);
}

void TestRook::test_horizontal()
{
	/*
	 *Test Rook horizontal movement
	 *move right
	 */
	CPPUNIT_ASSERT_NO_THROW(Rook->movement->path(c00, c02));
	///move left
	CPPUNIT_ASSERT_NO_THROW(Rook->movement->path(c02, c00));
}

void TestRook::test_vertical()
{
	/*
	 *Rook PATH TESTING
	 *Test Rook vertical movement up
	 */
	CPPUNIT_ASSERT_NO_THROW(Rook->movement->path(c20,c00));

	///Test Rook vertical movement down
	CPPUNIT_ASSERT_NO_THROW(Rook->movement->path(c00, c20));
}


void TestRook::test_rook()
{
	test_vertical();

	test_horizontal();

	test_diagonal();
}
