
#include "Chess.h"
#include "TestGame.h"
#include "KingMovement.h"

#include "Exceptions.h"
#include <iostream>
#include <typeinfo>
#include <string>

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestGame );

void TestGame::setUp()
{
	chess = new Chess();
}


void TestGame::tearDown()
{
	delete chess;
}

void TestGame::test_Chess_Constructor()
{
	///Test to ensure that board was created with proper size
	CPPUNIT_ASSERT_EQUAL(6, chess->board->width);
	CPPUNIT_ASSERT_EQUAL(6, chess->board->height);
}

void TestGame::test_Chess_SetUp()
{
	chess->setup();
	///Check that white goes first and that numPlayers is correct
	CPPUNIT_ASSERT_EQUAL(0, Game::turn);
	CPPUNIT_ASSERT_EQUAL(2, Game::numPlayers);
///CHECK THAT SQUARES WHICH SHOULD BE OCCUPIED ARE ALIVE
	for(int i = 0; i < 2; i++)
	{
		for(int j = 0; j < 6; j++)
		{
			CPPUNIT_ASSERT_EQUAL(true, chess->board->getSquare(i,j)->getPiece()->isAlive());
		}
	}
	for(int i = 4; i < 6; i++)
	{
		for(int j = 0; j < 6; j++)
		{
			CPPUNIT_ASSERT_EQUAL(true, chess->board->getSquare(i,j)->getPiece()->isAlive());
		}
	}
///CHECK EMPTY SQUARES
	/*
	 * Test that every square contains the right piece after setup() has been called
	 * Check all empty squares first. Make sure they do not hold a piece.
	 */
	for(int i = 2; i < 4; i++)
	{
		for(int j = 0; j < 6; j++)
		{
			CPPUNIT_ASSERT(chess->board->getSquare(i,j)->getPiece() == NULL);
		}
	}

///CHECK THAT SYMBOLS WERE PROPERLY ASSIGNED TO PIECES

	///Check that row 1 contains all white pawns and row 4 contains all black pawns
	for(int i = 0; i < 6; i++)
	{
		///Check white pawns are properly placed
		CPPUNIT_ASSERT_EQUAL('p', chess->board->getSquare(1,i)->symbol());
		///Check black pawns are properly placed
		CPPUNIT_ASSERT_EQUAL('P', chess->board->getSquare(4,i)->symbol());
	}

	///Check rooks are placed in corners
	CPPUNIT_ASSERT_EQUAL('R', chess->board->getSquare(5,0)->symbol());
	CPPUNIT_ASSERT_EQUAL('R', chess->board->getSquare(5,5)->symbol());
	CPPUNIT_ASSERT_EQUAL('r', chess->board->getSquare(0,0)->symbol());
	CPPUNIT_ASSERT_EQUAL('r', chess->board->getSquare(0,5)->symbol());

	///Check bishops are placed in proper spots
	CPPUNIT_ASSERT_EQUAL('B', chess->board->getSquare(5,4)->symbol());
	CPPUNIT_ASSERT_EQUAL('B', chess->board->getSquare(5,1)->symbol());
	CPPUNIT_ASSERT_EQUAL('b', chess->board->getSquare(0,1)->symbol());
	CPPUNIT_ASSERT_EQUAL('b', chess->board->getSquare(0,4)->symbol());	

	///Check queens are properly placed
	CPPUNIT_ASSERT_EQUAL('Q', chess->board->getSquare(5,3)->symbol());
	CPPUNIT_ASSERT_EQUAL('q', chess->board->getSquare(0,3)->symbol());
	
	///Check that kings are properly placed
	CPPUNIT_ASSERT_EQUAL('K', chess->board->getSquare(5,2)->symbol());
	CPPUNIT_ASSERT_EQUAL('k', chess->board->getSquare(0,2)->symbol());	
}


void TestGame::test_Chess_IsOver()
{
	CPPUNIT_ASSERT_EQUAL(false, chess->isOver());
	chess->board->getSquare(0,2)->getPiece()->kill();
	CPPUNIT_ASSERT_EQUAL(true, chess->isOver());
}


void TestGame::test_Chess_getSquare()
{
	///Check that no exception is thrown for valid inputs
	std::string coordValues = "0 0";
	std::istringstream iss (coordValues);
	std::string a;
	std::string b;
	CPPUNIT_ASSERT_NO_THROW(chess->getSquare(iss));
	for(int i = 0; i < 6; i++)
	{
		a = std::to_string(i);
		b = std::to_string(i);
		coordValues = a + " " + b;
		std::istringstream iss (coordValues);
		CPPUNIT_ASSERT_NO_THROW(chess->getSquare(iss));
	}

	///Check that exception is thrown for row >= SIZE
	coordValues = "6 0";
	CPPUNIT_ASSERT_THROW(chess->getSquare(iss), invalid_format_error);

	///Check that exception is thrown for col >= SIZE
	coordValues = "0 6";
	CPPUNIT_ASSERT_THROW(chess->getSquare(iss), invalid_format_error);

}


void TestGame::test_Game()
{
	test_Chess_Constructor();
	test_Chess_SetUp();
	test_Chess_IsOver();
	test_Chess_getSquare();
}