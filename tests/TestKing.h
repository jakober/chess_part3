#ifndef TESTKING_H
#define TESTKING_H

#include <cppunit/extensions/HelperMacros.h>


class TestKing : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( TestKing );
  CPPUNIT_TEST( test_King );
  CPPUNIT_TEST_SUITE_END();

public:

  void setUp();
  void tearDown();
  Piece *King;
  Coord *c00, *c10, *c20, *c01, *c11, *c21, *c02, *c12, *c22;

  void test_horizontal_one_space();

  void test_vertical_one_space();

  void test_diagonal_one_space();

  void test_horizontal_two_spaces();

  void test_vertical_two_spaces();

  void test_diagonal_two_spaces(); 


  void test_King();

};

#endif  // TESTKING_H