#ifndef TESTBISHOP_H
#define TESTBISHOP_H

#include <cppunit/extensions/HelperMacros.h>


class TestBishop : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( TestBishop );
  CPPUNIT_TEST( test_Bishop );
  CPPUNIT_TEST_SUITE_END();

public:

  void setUp();
  void tearDown();
  Piece *Bishop;
  Coord *c00, *c10, *c20, *c01, *c11, *c21, *c02, *c12, *c22;

  void test_horizontal();

  void test_vertical();

  void test_valid_move();

  void test_Bishop();

};

#endif  // TESTBISHOP_H