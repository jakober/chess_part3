#ifndef TESTQUEEN_H
#define TESTQUEEN_H

#include <cppunit/extensions/HelperMacros.h>


class TestQueen : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( TestQueen );
  CPPUNIT_TEST( test_Queen );
  CPPUNIT_TEST_SUITE_END();

public:

  void setUp();
  void tearDown();
  Piece *Queen;
  Coord *c00, *c10, *c20, *c01, *c11, *c21, *c02, *c12, *c22;

  void test_horizontal();

  void test_vertical();

  void test_diagonal();

  void test_invalid_diagonal();

  void test_Queen();

};

#endif  // TESTQUEEN_H