#include "KingMovement.h"

#include "Game.h"
#include "TestKing.h"

#include "Exceptions.h"
#include <iostream>
#include <vector>

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestKing );

void TestKing::setUp()
{
	King = new Piece(Piece::black, 'Q', new KingMovement);
	c00 = new Coord(0,0);
	c10 = new Coord(1,0);
	c20 = new Coord(2,0);
	c01 = new Coord(0,1);
	c11 = new Coord(1,1);
	c21 = new Coord(2,1);
	c02 = new Coord(0,2);
	c12 = new Coord(1,2);
	c22 = new Coord(2,2);

}


void TestKing::tearDown()
{
	delete King;
	delete c00;
	delete c10;
	delete c20;
	delete c01;
	delete c11;
	delete c21;
	delete c02;
	delete c12;
	delete c22;

}

void TestKing::test_horizontal_one_space()
{
	///Test King horizontal movement
	///move right
	CPPUNIT_ASSERT_NO_THROW(King->movement->path(c10, c11));
	///move left
	CPPUNIT_ASSERT_NO_THROW(King->movement->path(c11, c10));
}

void TestKing::test_vertical_one_space()
{
	/*
	  King PATH TESTING
	 *Test King vertical movement forward one space
	 */
	CPPUNIT_ASSERT_NO_THROW(King->movement->path(c20,c10));
	///backward one space
	CPPUNIT_ASSERT_NO_THROW(King->movement->path(c10,c20));
}

void TestKing::test_diagonal_one_space()
{
	///Test King diagonal movement
	///up and left
	CPPUNIT_ASSERT_NO_THROW(King->movement->path(c11, c00));
	///up and right
	CPPUNIT_ASSERT_NO_THROW(King->movement->path(c11, c02));
	///down and left
	CPPUNIT_ASSERT_NO_THROW(King->movement->path(c11, c20));
	///down and right
	CPPUNIT_ASSERT_NO_THROW(King->movement->path(c11, c22));
}

void TestKing::test_horizontal_two_spaces()
{
	///two spaces right
	CPPUNIT_ASSERT_THROW(King->movement->path(c00, c02), invalid_move_error);
	///two spaces left
	CPPUNIT_ASSERT_THROW(King->movement->path(c02, c00), invalid_move_error);

}

void TestKing::test_vertical_two_spaces()
{
	///Test King vertical movement forward two spaces
	CPPUNIT_ASSERT_THROW(King->movement->path(c20, c00), invalid_move_error);
	///Test King vertical movement backward two spaces
	CPPUNIT_ASSERT_THROW(King->movement->path(c00, c20), invalid_move_error);
}

void TestKing::test_diagonal_two_spaces()
{
	///two spaces up and right
	CPPUNIT_ASSERT_THROW(King->movement->path(c20, c02), invalid_move_error);
	///two spaces up and left
	CPPUNIT_ASSERT_THROW(King->movement->path(c22, c00), invalid_move_error);
	///two spaces down and right
	CPPUNIT_ASSERT_THROW(King->movement->path(c00, c22), invalid_move_error);
	///two spaces down and left
	CPPUNIT_ASSERT_THROW(King->movement->path(c02, c20), invalid_move_error);
} 


void TestKing::test_King()
{
	test_horizontal_one_space();

	test_vertical_one_space();

	test_diagonal_one_space();

	test_horizontal_two_spaces();

	test_vertical_two_spaces();

	test_diagonal_two_spaces(); 
}