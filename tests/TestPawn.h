#ifndef TESTPAWN_H
#define TESTPAWN_H

#include <cppunit/extensions/HelperMacros.h>


class TestPawn : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( TestPawn );
  CPPUNIT_TEST( test_Pawn );
  CPPUNIT_TEST_SUITE_END();

public:

  void setUp();
  void tearDown();
  Piece *whitePawn, *blackPawn;
  Coord *c00, *c10, *c20, *c01, *c11, *c21, *c02, *c12, *c22;

  void test_horizontal();

  void test_forward_two_spaces();

  void test_backwards();

  void test_diagonal();

  void test_valid_move();

  void test_Pawn();

};

#endif  // TESTPAWN_H