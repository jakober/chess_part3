#include "PawnMovement.h"

#include "Game.h"
#include "TestPawn.h"

#include "Exceptions.h"
#include <iostream>
#include <vector>

/// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestPawn );

void TestPawn::setUp()
{
	blackPawn = new Piece(Piece::black, 'P', new BlackPawnMovement);
	whitePawn = new Piece(Piece::white, 'p', new WhitePawnMovement);
	c00 = new Coord(0,0);
	c10 = new Coord(1,0);
	c20 = new Coord(2,0);
	c01 = new Coord(0,1);
	c11 = new Coord(1,1);
	c21 = new Coord(2,1);
	c02 = new Coord(0,2);
	c12 = new Coord(1,2);
	c22 = new Coord(2,2);

}


void TestPawn::tearDown()
{
	delete whitePawn;
	delete blackPawn;
	delete c00;
	delete c10;
	delete c20;
	delete c01;
	delete c11;
	delete c21;
	delete c02;
	delete c12;
	delete c22;

}

void TestPawn::test_horizontal()
{
	///Test black pawn horizontal movement
	///move right
	CPPUNIT_ASSERT_THROW(blackPawn->movement->path(c10, c11), invalid_move_error);
	///move left
	CPPUNIT_ASSERT_THROW(blackPawn->movement->path(c11, c10), invalid_move_error);

	///Test white pawn horizontal movement
	///move right
	CPPUNIT_ASSERT_THROW(whitePawn->movement->path(c10, c11), invalid_move_error);
	///move left
	CPPUNIT_ASSERT_THROW(whitePawn->movement->path(c11, c10), invalid_move_error);


}

void TestPawn::test_forward_two_spaces()
{
	///Test black pawn vertical movement forward two spaces
	CPPUNIT_ASSERT_THROW(blackPawn->movement->path(c20, c00), invalid_move_error);

	///Test white pawn vertical movement forward two spaces
	CPPUNIT_ASSERT_THROW(whitePawn->movement->path(c00, c20), invalid_move_error);

}

void TestPawn::test_backwards()
{
	///Test black pawn vertical movement backwards
	CPPUNIT_ASSERT_THROW(blackPawn->movement->path(c10, c20), invalid_move_error);

	///Test white pawn vertical movement backwards
	CPPUNIT_ASSERT_THROW(whitePawn->movement->path(c10, c00), invalid_move_error);
}

void TestPawn::test_diagonal()
{
	///Test black pawn diagonal movement
	///up and left
	CPPUNIT_ASSERT_THROW(blackPawn->movement->path(c11, c00), invalid_move_error);
	///up and right
	CPPUNIT_ASSERT_THROW(blackPawn->movement->path(c11, c02), invalid_move_error);
	///down and left
	CPPUNIT_ASSERT_THROW(blackPawn->movement->path(c11, c20), invalid_move_error);
	///down and right
	CPPUNIT_ASSERT_THROW(blackPawn->movement->path(c11, c22), invalid_move_error);

	///Test white pawn diagonal movement
	///up and left
	CPPUNIT_ASSERT_THROW(whitePawn->movement->path(c11, c00), invalid_move_error);
	///up and right
	CPPUNIT_ASSERT_THROW(whitePawn->movement->path(c11, c02), invalid_move_error);
	///down and left
	CPPUNIT_ASSERT_THROW(whitePawn->movement->path(c11, c20), invalid_move_error);
	///down and right
	CPPUNIT_ASSERT_THROW(whitePawn->movement->path(c11, c22), invalid_move_error);
}

void TestPawn::test_valid_move()
{
	/*
	 *Test black pawn vertical movement forward one space
	 */
	CPPUNIT_ASSERT_NO_THROW(blackPawn->movement->path(c20,c10));

	/*
	 *Test white pawn vertical movement forward one space
	 */	
	CPPUNIT_ASSERT_NO_THROW(whitePawn->movement->path(c10,c20));
}



void TestPawn::test_Pawn()
{
	test_horizontal();

	test_forward_two_spaces();

	test_backwards();

	test_diagonal();

	test_valid_move();
}
