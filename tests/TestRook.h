#ifndef TESTROOK_H
#define TESTROOK_H

#include <cppunit/extensions/HelperMacros.h>


class TestRook : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE( TestRook );
  CPPUNIT_TEST( test_rook );
  //CPPUNIT_TEST_EXCEPTION( testInvalidMoveThrows, invalid_move_error );
  //CPPUNIT_TEST( test_Invalid_Move_Throws );
  CPPUNIT_TEST_SUITE_END();

public:

  void setUp();
  void tearDown();
  Piece *Rook;
  Coord *c00, *c10, *c20, *c01, *c11, *c21, *c02, *c12, *c22;

  void test_diagonal();

  void test_horizontal();

  void test_vertical();

  void test_rook();

};

#endif  // TESTROOK_H