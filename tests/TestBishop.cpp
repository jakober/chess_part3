#include "BishopMovement.h"

#include "Game.h"
#include "TestBishop.h"

#include "Exceptions.h"
#include <iostream>
#include <vector>

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestBishop );

void TestBishop::setUp()
{
	Bishop = new Piece(Piece::black, 'B', new BishopMovement);
	c00 = new Coord(0,0);
	c10 = new Coord(1,0);
	c20 = new Coord(2,0);
	c01 = new Coord(0,1);
	c11 = new Coord(1,1);
	c21 = new Coord(2,1);
	c02 = new Coord(0,2);
	c12 = new Coord(1,2);
	c22 = new Coord(2,2);

}


void TestBishop::tearDown()
{
	delete Bishop;
	delete c00;
	delete c10;
	delete c20;
	delete c01;
	delete c11;
	delete c21;
	delete c02;
	delete c12;
	delete c22;

}
void TestBishop::test_horizontal()
{
	///Test Bishop horizontal movement right and left
	CPPUNIT_ASSERT_THROW(Bishop->movement->path(c11, c12), invalid_move_error);
	CPPUNIT_ASSERT_THROW(Bishop->movement->path(c11, c10), invalid_move_error);
}

void TestBishop::test_vertical()
{
	///Test Bishop vertical movement up and down
	CPPUNIT_ASSERT_THROW(Bishop->movement->path(c11, c01), invalid_move_error);
	CPPUNIT_ASSERT_THROW(Bishop->movement->path(c11, c21), invalid_move_error);
}

void TestBishop::test_valid_move()
{
	/*
	 *Test Bishop diagonal movement down and right
	 */
	CPPUNIT_ASSERT_NO_THROW(Bishop->movement->path(c00,c22));

	///Test Bishop diagonal movement up and left
	CPPUNIT_ASSERT_NO_THROW(Bishop->movement->path(c22, c00));

	///Test Bishop diagonal movement up and right
	CPPUNIT_ASSERT_NO_THROW(Bishop->movement->path(c20, c02));

	///Test Bishop diagonal movement down and left
	CPPUNIT_ASSERT_NO_THROW(Bishop->movement->path(c02, c20));
}


void TestBishop::test_Bishop()
{
	test_horizontal();
	test_vertical();
	test_valid_move();
}