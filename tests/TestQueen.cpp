#include "QueenMovement.h"

#include "Game.h"
#include "TestQueen.h"

#include "Exceptions.h"
#include <iostream>
#include <vector>

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION( TestQueen );

void TestQueen::setUp()
{
	Queen = new Piece(Piece::black, 'Q', new QueenMovement);
	c00 = new Coord(0,0);
	c10 = new Coord(1,0);
	c20 = new Coord(2,0);
	c01 = new Coord(0,1);
	c11 = new Coord(1,1);
	c21 = new Coord(2,1);
	c02 = new Coord(0,2);
	c12 = new Coord(1,2);
	c22 = new Coord(2,2);

}


void TestQueen::tearDown()
{
	delete Queen;
	delete c00;
	delete c10;
	delete c20;
	delete c01;
	delete c11;
	delete c21;
	delete c02;
	delete c12;
	delete c22;

}

void TestQueen::test_horizontal()
{
	///Test Queen horizontal movement right and left
	CPPUNIT_ASSERT_NO_THROW(Queen->movement->path(c11, c12));
	CPPUNIT_ASSERT_NO_THROW(Queen->movement->path(c11, c10));
}

void TestQueen::test_vertical()
{
	///Test Queen vertical movement up and down
	CPPUNIT_ASSERT_NO_THROW(Queen->movement->path(c11, c01));
	CPPUNIT_ASSERT_NO_THROW(Queen->movement->path(c11, c21));
}

void TestQueen::test_diagonal()
{
	/*
	  Queen PATH TESTING
	 *Test Queen diagonal movement down and right
	 */
	CPPUNIT_ASSERT_NO_THROW(Queen->movement->path(c00,c22));

	///Test Queen diagonal movement up and left
	CPPUNIT_ASSERT_NO_THROW(Queen->movement->path(c22, c00));

	///Test Queen diagonal movement up and right
	CPPUNIT_ASSERT_NO_THROW(Queen->movement->path(c20, c02));

	///Test Queen diagonal movement down and left
	CPPUNIT_ASSERT_NO_THROW(Queen->movement->path(c02, c20));
}

void TestQueen::test_invalid_diagonal()
{
	///Test Queen movement that is neither horizontal, vertical, nor diagonal
	CPPUNIT_ASSERT_THROW(Queen->movement->path(c00, c21), invalid_move_error);
	CPPUNIT_ASSERT_THROW(Queen->movement->path(c02, c10), invalid_move_error);	
	CPPUNIT_ASSERT_THROW(Queen->movement->path(c21, c02), invalid_move_error);
	CPPUNIT_ASSERT_THROW(Queen->movement->path(c01, c22), invalid_move_error);	
	
}

void TestQueen::test_Queen()
{
	test_diagonal();

	test_vertical();

	test_horizontal();

	test_invalid_diagonal();
}