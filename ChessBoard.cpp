/*
 * ChessBoard.cpp
 *
 *  Created on: Oct 7, 2015
 *      Author: anvik
 */

#include "ChessBoard.h"
#include "Game.h"
#include "Exceptions.h"

ChessBoard::ChessBoard(const int w, const int h) :
		Board(w, h) {
}

void ChessBoard::movePiece(Square* s, Square* d) const {
	/// Call base class version for generic validation
	/*
	 *We need to maintain access to whatever piece, if any, was on the destination square.
	 *Otherwise, if the user enters coordinates that are within the Board's boundaries, but
	 *would throw an error within ChessBoard's movePiece(), then Board's movePiece() will still
	 *execute the move, since it successfully passes all of the error checking in Board's
	 *movePiece(). This WAS a BUG. This is no longer a bug :).
	 */
	Piece *temp = d->getPiece();
	

	Board::movePiece(s, d);

	// Move piece back in case there is an issue
	Piece* p = board[d->row][d->col].getPiece();
	board[s->row][s->col].setPiece(p);
	board[d->row][d->col].removePiece();
	board[d->row][d->col].setPiece(temp);

	// Check it is player's piece to move
	if (p->player == Piece::white && Game::turn % 2 != 0)
		throw invalid_move_error("Black moving white");
	if (p->player == Piece::black && Game::turn % 2 != 1)
		throw invalid_move_error("White moving black");

	/*
	 *Check that there is nothing in the way
	 *I switched row and column since they were backward and made testing path() very difficult.
	 */
	const Coord* sCoord = new Coord(s->row, s->col);
	const Coord* eCoord = new Coord(d->row, d->col);

	Movement* m = p->movement;
	std::vector<const Coord*> piecePath = m->path(sCoord, eCoord);
	for (unsigned i = 1; i < piecePath.size() - 1; i++) {
		const Coord* c = piecePath.at(i);
		///Switched c->x and c->y since they were backwards and made testing path difficult
		Piece* p = getSquare(c->x, c->y)->getPiece();		
		if (p != NULL)
		{
			///Must delete before we throw, since deletion calls will not be executed after throw
			for (unsigned int i = 0; i < piecePath.size(); i++)
				delete piecePath.at(i);
			delete sCoord;
			delete eCoord;
			throw invalid_move_error("Cannot jump over pieces.");
		}
	}

	/*
	 *Note: All Movements for all pieces now only push new coordinates, they do not ever push_back sCoord
	 *or eCoord. This allows for consistent deletion since the pointers in path vector and sCoord/eCoord never
	 *point to the same Coordinate. Also, piece movement path methods are not responsible for the deletion
	 *of sCoord or eCoord. Lastly, we delete them here, if the move was deemed valid, but also in the 
	 *throw statement above, since they also need to be deleted should the move be deemed invalid.
	 */
	for (unsigned int i = 0; i < piecePath.size(); i++)
		delete piecePath.at(i);
	delete sCoord;
	delete eCoord;

	if(board[d->row][d->col].getPiece()!=NULL)
	{
		Piece *killPiece = board[d->row][d->col].getPiece();
		killPiece->kill();
	}
	board[d->row][d->col].setPiece(p);
	board[s->row][s->col].removePiece();

}
