/*
 * BishopMovement.cpp
 *
 *  Created on: Sep 17, 2015
 *      Author: anvik
 */

#include "BishopMovement.h"
#include "Exceptions.h"

std::vector<const Coord*> BishopMovement::path(const Coord* s, const Coord* e) {

	std::vector<const Coord*> path;	
	int rowDiff = (e->x - s->x);
	int colDiff = (e->y - s->y);

	if (abs(rowDiff) != abs(colDiff))
	{
		throw invalid_move_error("Bishops can only move diagonally");
	}
	int rowCount = s->x;
	int colCount = s->y;
	//path.push_back(s);
	path.push_back(new Coord(s->x, s->y));
///HANDLE DIAGONAL MOVEMENT
	if(abs(colDiff) == abs(rowDiff))
	{


	///Handles cases like (0,0) to (4,4)
		if((colDiff > 0) && (rowDiff > 0))
		{
			for(int i = 0; i < colDiff; i++)
			{
				Coord* c = new Coord(rowCount+1, colCount + 1);
				path.push_back(c);
				rowCount++;
				colCount++;
			}
		}
	///Handles cases like (4,4) to (0,0)
		else if((colDiff < 0) && (rowDiff < 0))
		{
			for(int i = 0; i < abs(rowDiff); i++)
			{
				Coord* c = new Coord(rowCount-1, colCount-1);
				path.push_back(c);
				rowCount--;
				colCount--;
			}
		}

	///Handles cases like (4,1) to (1,4)
		else if((rowDiff < 0) && (colDiff > 0))
		{
			for(int i = 0; i<abs(rowDiff); i++)
			{
				Coord* c = new Coord(rowCount-1, colCount+1);
				path.push_back(c);
				rowCount--;
				colCount++;
			}

		}
	///Handles cases like (1,4) to (4,1)	
		else
		{
			for(int i = 0; i<abs(rowDiff); i++)
			{
				Coord* c = new Coord(rowCount+1, colCount-1);
				path.push_back(c);
				rowCount++;
				colCount--;
			}
		}

	}
	return path;
}

